**Java Data access with JDBC**
--
The main idea
--
1. Write SQL scripts (inside folder SQL)
2. Build a Spring Boot application in Java (inside folder src).
3. Javadoc for each method, explaining its purpose and what data it returns

Set up needed
--
- Intellij Ultimate
- Postgres and PgAdmin
- Java 17
- Postgres SQL driver dependency

Maintainers
--
 - George Pegias
 - Aris Kardasis