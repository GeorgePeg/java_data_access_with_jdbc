/*********************************************************************************************
Updating data
-------------
Pick one superhero to update their data.

REQUIREMENT: Create a script called 07_updateSuperhero.sql where you can update a superheroes 
-----------  name. Pick any superhero to do this with.
*********************************************************************************************/
UPDATE public.superhero
SET sup_name = 'Bruce Wayne'
WHERE sup_name = 'Peter Parker'; 