/*********************************************************************************************
Deleting data
-------------
Referential integrity will stop us from deleting any records which have got relationships used. 
Meaning, a superhero cannot be deleted if it is used in the assistant table.

REQUIREMENT: Create a script called 08_deleteAssistant.sql where you can delete any assistant. 
-----------  You can delete that assistant by name (his name must be unique to avoid deleting 
             multiple assistants), this is done to ease working with autoincremented numbers 
			 – my PC may skip a number or two. 

NOTE: The scripts should be able to be run in order with no errors to create a database with 
----  the requirements stated. That is how I will initially check for functionality, then I 
      will investigate each statement for specifics.
*********************************************************************************************/
DELETE FROM assistant WHERE ass_name = 'Gwen Stacy';

