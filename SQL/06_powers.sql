/*********************************************************************************************
Inserting data (3rd part)
--------------
Finally, we need to add powers. This requires you to add the powers first, then associate them 
with Superheroes.

REQUIREMENT: Create a script called 06_powers.sql that inserts four powers. Then the script 
-----------  needs to give the superheroes powers. Try have one superhero with multiple powers 
             and one power that is used by multiple superheroes, to demonstrate the many-to-many.
*********************************************************************************************/
INSERT INTO heroPower(pow_name, pow_description) 
VALUES ('Fly', 'User can move through the air');
INSERT INTO heroPower(pow_name, pow_description) 
VALUES ('Super Strength', 'User can lift weights beyond any ordinary human');
INSERT INTO heroPower(pow_name, pow_description) 
VALUES ('Super Speed', 'User can run beyond any ordinary human');
INSERT INTO heroPower(pow_name, pow_description) 
VALUES ('Frost Breath', 'User can blow super cold air');

INSERT INTO powered (heropower_id, superhero_id) VALUES (1, 1);
INSERT INTO powered (heropower_id, superhero_id) VALUES (1, 3);
INSERT INTO powered (heropower_id, superhero_id) VALUES (2, 1);
INSERT INTO powered (heropower_id, superhero_id) VALUES (2, 2);
INSERT INTO powered (heropower_id, superhero_id) VALUES (2, 3);
INSERT INTO powered (heropower_id, superhero_id) VALUES (3, 1);
INSERT INTO powered (heropower_id, superhero_id) VALUES (3, 3);
INSERT INTO powered (heropower_id, superhero_id) VALUES (4, 1);
