/*********************************************************************************************
Inserting data (2nd part)
--------------
Then we need to add assistant data.

REQUIREMENT: Create a script called 05_insertAssistants.sql that inserts three assistants, 
-----------  decide on which superheroes these can assist.
*********************************************************************************************/
INSERT INTO assistant (ass_name, superhero_id) VALUES ('Lois Lane', 1);
INSERT INTO assistant (ass_name, superhero_id) VALUES ('Jimmy Olsen', 1);
INSERT INTO assistant (ass_name, superhero_id) VALUES ('Gwen Stacy', 2);
