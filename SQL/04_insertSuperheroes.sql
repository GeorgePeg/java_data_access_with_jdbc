/*********************************************************************************************
Inserting data (1st part)
--------------
Now it is time to populate the database using INSERT statements. Firstly, we should add 
superheroes.

REQUIREMENT: Create a script called 04_insertSuperheroes.sql that inserts three new superheroes 
-----------  into the database.
*********************************************************************************************/

INSERT INTO superhero (sup_name, sup_alias, sup_origin) VALUES ('Clark Kent', 'Superman', 'Krypton');
INSERT INTO superhero (sup_name, sup_alias, sup_origin) VALUES ('Peter Parker', 'Spiderman', 'Earth');
INSERT INTO superhero (sup_name, sup_alias, sup_origin) VALUES ('Thor Odinson', 'Thor', 'Asgard');
