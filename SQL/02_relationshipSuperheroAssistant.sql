/*********************************************************************************************
Table relationships (1st part)
-------------------
Here we are going to include some relationships between the tables. The ALTER SQL keyword will 
be used to change the existing tables to add the keys. A description of the relationships can 
be seen below:

    • One Superhero can have multiple assistants, one assistant has one superhero they assist.
    • One Superhero can have many powers, and one power can be present on many Superheroes.
	
Firstly, to setup the Superhero-assistant relationship, we need to add a foreign key. This 
represents the superhero this assistant is linked to (SuperheroId in Assistant), and ALTER any 
tables needed to cater for this foreign key, and to setup the constraint.

REQUIREMENT: Create a script called 02_relationshipSuperheroAssistant.sql that contains 
-----------  statements to ALTER any tables needed to add the foreign key and setup the 
             constraint to configure the described relationship between Superhero and assistant.
*********************************************************************************************/
ALTER TABLE assistant
ADD superhero_id int REFERENCES superhero;
    