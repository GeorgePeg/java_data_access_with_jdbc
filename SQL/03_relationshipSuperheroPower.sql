/*********************************************************************************************
Table relationships (2nd part)
-------------------
Finally, to setup the Superhero-power relationship, we need to add a linking table. The name 
of this table is up to you but should be based on convention. This table is there purely for 
linking, meaning it needs to contain only two fields, which are both foreign keys and a 
composite primary key.

REQUIREMENT: Create a script called 03_relationshipSuperheroPower.sql that contains statements 
-----------  to create the linking table. This script should contain any ALTER statements 
             needed to set up the foreign key constraints between the linking tables and the 
			 Superhero and Power tables.
*********************************************************************************************/
CREATE TABLE powered(
	heropower_id int REFERENCES heroPower,
	superhero_id int REFERENCES superhero,
	PRIMARY KEY(heropower_id, superhero_id)
);
