/*********************************************************************************************
Tables
------
There are three main tables you need to create, this is Superhero, Assistant, and Power.
                                                        
Superhero has: Autoincremented integer Id, Name, Alias, Origin.
Assistant has: Autoincremented integer Id, Name.
Power has: Autoincremented integer Id, Name, Description.

REQUIREMENT: Create a script called 01_tableCreate.sql that contains statements to create 
-----------  each of these tables and setup their primary keys. 

NOTE: At this point in time, no relationships are to be configured aside from primary keys.
----  Datatypes should be up to you, and what you think is best.
*********************************************************************************************/
DROP TABLE IF EXISTS superhero;

CREATE TABLE superhero(
	sup_id serial PRIMARY KEY,
	sup_name varchar(30), 
	sup_alias varchar(10), 
	sup_origin varchar(20)
);

DROP TABLE IF EXISTS assistant;

CREATE TABLE assistant(
	ass_id serial PRIMARY KEY,
	ass_name varchar(30)
);

DROP TABLE IF EXISTS heroPower;

CREATE TABLE heroPower(
	pow_id serial PRIMARY KEY,
	pow_name varchar(30),
	pow_description varchar(50)
);
