package com.accelerate.itunes_app.Models;

public record CustomerSpender(int customer_id,
                              String first_name,
                              String last_name,
                              double total) {
}
