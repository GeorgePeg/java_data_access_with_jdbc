package com.accelerate.itunes_app.Models;

public record CustomerGenre(int genre_id,
                            String genre_name,
                            int occurences) {
}
