package com.accelerate.itunes_app.Models;

public record CustomerCountry(String country,
                              int occurences) {
}

