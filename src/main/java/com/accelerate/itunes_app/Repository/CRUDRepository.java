package com.accelerate.itunes_app.Repository;

import org.springframework.stereotype.Repository;
import java.util.List;
@Repository
public interface CRUDRepository<T, U>{
    /**
     * @param customer Mainly customer's information
     * @return Return 1 if customer added successfully else 0
     */
    int insert(T customer);
    /**
     * @param customer Mainly customer's information
     * @return Return 1 if customer modified successfully else 0
     */
    int update(T customer);
    /**
     * @param id Customer's unique number
     * @return All customers with the given id
     */
    List<T> getById(U id);
}