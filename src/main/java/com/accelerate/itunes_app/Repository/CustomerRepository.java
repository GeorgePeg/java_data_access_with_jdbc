package com.accelerate.itunes_app.Repository;

import com.accelerate.itunes_app.Models.Customer;
import com.accelerate.itunes_app.Models.CustomerCountry;
import com.accelerate.itunes_app.Models.CustomerGenre;
import com.accelerate.itunes_app.Models.CustomerSpender;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CustomerRepository extends CRUDRepository<Customer, Integer>{
    /**
     * @param id Customer's unique number
     * @return All customers with the given id
     */
    List<Customer> getById(Integer id);
    /**
     * @return All customers from table customer
     */
    List<Customer> getAll();
    /**
     * @param name Customer's name
     * @return A list of customers that have the given name
     */
    List<Customer> getByName(String name);
    /**
     * @param limit Maximum number of customers to return
     * @param offset Number of customers to skip
     * @return A list of customers depending on the number by limit and skipping by offset
     */
    List<Customer> getAPage(int limit, int offset);
    /**
     * @return The country with most occurrences
     */
    List<CustomerCountry> getCountryWithMostOccurences();
    /**
     * @return A list of customers with most money spend
     */
    List<CustomerSpender> getHighestSpender();
    /**
     * @param id Customer's unique number
     * @return Most popular genre for a customer by id
     */
    List<CustomerGenre> getMostPopularGenreByCustomerID(int id);
}
