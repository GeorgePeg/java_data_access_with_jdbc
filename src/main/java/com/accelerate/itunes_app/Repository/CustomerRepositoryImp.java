package com.accelerate.itunes_app.Repository;

import com.accelerate.itunes_app.Models.Customer;
import com.accelerate.itunes_app.Models.CustomerCountry;
import com.accelerate.itunes_app.Models.CustomerGenre;
import com.accelerate.itunes_app.Models.CustomerSpender;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class CustomerRepositoryImp implements CustomerRepository{
    private final String url;
    private final String username;
    private final String password;
    public CustomerRepositoryImp(@Value("${spring.datasource.url}") String url,
                                 @Value("${spring.datasource.username}") String username,
                                 @Value("${spring.datasource.password}") String password) {
        this.url = url;
        this.username = username;
        this.password = password;
    }
    @Override
    public List<Customer> getById(Integer id) {
        String sql = "SELECT customer_id, first_name, last_name, country, postal_code, phone, email " +
                     "FROM customer WHERE customer_id = ?";
        List<Customer> customers = new ArrayList<>();
        Customer customer = null;
        try (Connection conn = DriverManager.getConnection(url, username, password)){
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1, id);
            ResultSet result = statement.executeQuery();
            while (result.next()) {
                customer = new Customer(result.getInt("customer_id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("country"),
                        result.getString("postal_code"),
                        result.getString("phone"),
                        result.getString("email"));
                customers.add(customer);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        for(Customer c : customers)
            System.out.println(c);
        return customers;
    }
    @Override
    public int insert(Customer customer) {
        String sql = "INSERT INTO customer(customer_id, first_name, last_name, country, postal_code, phone, email) " +
                "VALUES(?,?,?,?,?,?,?)";
        int result = 0;
        try (Connection conn = DriverManager.getConnection(url, username, password)){
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1, customer.customer_id());
            statement.setString(2, customer.first_name());
            statement.setString(3, customer.last_name());
            statement.setString(4, customer.country());
            statement.setString(5, customer.postal_code());
            statement.setString(6, customer.phone());
            statement.setString(7, customer.email());
            result = statement.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        System.out.println(result);
        return result;
    }
    @Override
    public int update(Customer customer) {
        int result = 0;
        String sql = "UPDATE customer " +
                "SET first_name = ?, last_name = ?, country = ?, postal_code = ?, phone = ?, email = ? " +
                "WHERE customer_id = " + customer.customer_id();
        try (Connection conn = DriverManager.getConnection(url, username, password)){
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setString(1, customer.first_name());
            statement.setString(2, customer.last_name());
            statement.setString(3, customer.country());
            statement.setString(4, customer.postal_code());
            statement.setString(5, customer.phone());
            statement.setString(6, customer.email());
            result = statement.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        System.out.println(result);
        return result;
    }
    @Override
    public List<Customer> getAll() {
        String sql = "SELECT * FROM customer";
        List<Customer> customers = new ArrayList<>();
        Customer customer = null;
        try (Connection conn = DriverManager.getConnection(url, username, password)){
            PreparedStatement statement = conn.prepareStatement(sql);
            ResultSet result = statement.executeQuery();
            while (result.next()) {
                customer = new Customer(result.getInt("customer_id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("country"),
                        result.getString("postal_code"),
                        result.getString("phone"),
                        result.getString("email"));
                customers.add(customer);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        for(Customer c : customers)
            System.out.println(c);
        return customers;
    }
    @Override
    public List<Customer> getByName(String name) {
        String sql = "SELECT customer_id, first_name, last_name, country, postal_code, phone, email " +
                     "FROM customer " +
                     "WHERE first_name LIKE '%" + name + "%' OR last_name LIKE '%" + name + "%'";
        List<Customer> customers = new ArrayList<>();
        Customer customer = null;
        try (Connection conn = DriverManager.getConnection(url, username, password)){
            PreparedStatement statement = conn.prepareStatement(sql);
            ResultSet result = statement.executeQuery();
            while (result.next()) {
                customer = new Customer(result.getInt("customer_id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("country"),
                        result.getString("postal_code"),
                        result.getString("phone"),
                        result.getString("email"));
                customers.add(customer);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        for(Customer c : customers)
            System.out.println(c);
        return customers;
    }
    @Override
    public List<Customer> getAPage(int limit, int offset) {
        String sql = "SELECT customer_id, first_name, last_name, country, postal_code, phone, email " +
                     "FROM customer " +
                     "LIMIT " + limit + " OFFSET " + offset;
        List<Customer> customers = new ArrayList<>();
        Customer customer = null;
        try (Connection conn = DriverManager.getConnection(url, username, password)){
            PreparedStatement statement = conn.prepareStatement(sql);
            ResultSet result = statement.executeQuery();
            while (result.next()) {
                customer = new Customer(result.getInt("customer_id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("country"),
                        result.getString("postal_code"),
                        result.getString("phone"),
                        result.getString("email"));
                customers.add(customer);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        for(Customer c : customers)
            System.out.println(c);
        return customers;
    }
    @Override
    public List<CustomerCountry> getCountryWithMostOccurences() {
        String sql = "SELECT country, count(country) as occurences " +
                     "FROM customer " +
                     "GROUP BY country " +
                     "ORDER BY count(country) desc " +
                     "LIMIT 1";
        List<CustomerCountry> countries = new ArrayList<>();
        CustomerCountry country;
        try (Connection conn = DriverManager.getConnection(url, username, password)){
            PreparedStatement statement = conn.prepareStatement(sql);
            ResultSet result = statement.executeQuery();
            while (result.next()) {
                country = new CustomerCountry(result.getString("country"),
                                              result.getInt("occurences"));
                countries.add(country);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        for(CustomerCountry c: countries)
            System.out.println(c);
        return countries;
    }
    @Override
    public List<CustomerSpender> getHighestSpender() {
        String sql = "SELECT customer.customer_id, first_name, last_name, SUM(total) as total " +
                     "FROM customer INNER JOIN invoice ON customer.customer_id = invoice.customer_id " +
                     "GROUP BY customer.customer_id " +
                     "ORDER BY total desc " +
                     "LIMIT 1";
        List<CustomerSpender> spenders= new ArrayList<>();
        CustomerSpender spender;
        try (Connection conn = DriverManager.getConnection(url, username, password)){
            PreparedStatement statement = conn.prepareStatement(sql);
            ResultSet result = statement.executeQuery();
            while(result.next()){
                spender = new CustomerSpender(result.getInt("customer_id"),
                                              result.getString("first_name"),
                                              result.getString("last_name"),
                                              result.getDouble("total"));
                spenders.add(spender);
            }
        }catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        for(CustomerSpender s : spenders)
            System.out.println(s);
        return spenders;
    }
    @Override
    public List<CustomerGenre> getMostPopularGenreByCustomerID(int id) {
        String sql = "SELECT genre.genre_id, genre.name, COUNT(*) as occurences " +
                     "FROM genre INNER JOIN track ON genre.genre_id = track.genre_id" +
                               " INNER JOIN invoice_line ON track.track_id = invoice_line.track_id" +
                               " INNER JOIN invoice ON invoice_line.invoice_id = invoice.invoice_id" +
                               " INNER JOIN customer ON invoice.customer_id = customer.customer_id " +
                     "WHERE customer.customer_id = ? " +
                     "GROUP BY genre.genre_id " +
                     "ORDER BY COUNT(*) desc";
        CustomerGenre genre;
        List<CustomerGenre> genres = new ArrayList<>();
        try (Connection conn = DriverManager.getConnection(url, username, password)){
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1, id);
            ResultSet result = statement.executeQuery();
            if(result.next()){
                int occ = result.getInt("occurences");
                do{
                    if(occ!=result.getInt("occurences"))
                        break;
                    genre = new CustomerGenre(result.getInt("genre_id"),
                                              result.getString("name"),
                                              result.getInt("occurences"));
                    genres.add((genre));
                } while(result.next());
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        for(CustomerGenre g : genres)
            System.out.println(g);
        return genres;
    }
}