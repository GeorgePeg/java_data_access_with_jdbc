package com.accelerate.itunes_app.Runner;

import com.accelerate.itunes_app.Models.Customer;
import com.accelerate.itunes_app.Repository.CustomerRepositoryImp;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class CustomerRunner implements ApplicationRunner {
    private final CustomerRepositoryImp customerRepositoryImp;
    public CustomerRunner(CustomerRepositoryImp customerRepositoryImp){
        this.customerRepositoryImp = customerRepositoryImp;
    }
    @Override
    public void run(ApplicationArguments args) throws Exception {
        customerRepositoryImp.getAll();
        System.out.println("___________________________________________________________________");
        customerRepositoryImp.getById(9);
        System.out.println("___________________________________________________________________");
        customerRepositoryImp.getByName("Mark");
        System.out.println("___________________________________________________________________");
        customerRepositoryImp.getAPage(5, 50);
        System.out.println("___________________________________________________________________");
        customerRepositoryImp.insert(new Customer(61, "Aris", "Kardasis", "Greece", "55555", "756934244", "arisr@gmail.com"));
        System.out.println("___________________________________________________________________");
        customerRepositoryImp.update(new Customer(61, "George", "Pegias", "Turkey", "42412", "765437573", "ge@gmail.com"));
        System.out.println("___________________________________________________________________");
        customerRepositoryImp.getCountryWithMostOccurences();
        System.out.println("___________________________________________________________________");
        customerRepositoryImp.getHighestSpender();
        System.out.println("___________________________________________________________________");
        customerRepositoryImp.getMostPopularGenreByCustomerID(12);
    }
}
