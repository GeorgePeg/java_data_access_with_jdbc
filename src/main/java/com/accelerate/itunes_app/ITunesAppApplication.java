package com.accelerate.itunes_app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ITunesAppApplication{
    public static void main(String[] args) {
        SpringApplication.run(ITunesAppApplication.class, args);
    }
}
